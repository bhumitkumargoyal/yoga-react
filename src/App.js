import React, { Component } from 'react';
//import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/animate/animate.min.css';
import './css/flaticon/font/flaticon.css';
import './css/owlcarousel/assets/owl.carousel.min.css';
import './css/lightbox/css/lightbox.min.css';
import './css/style.css';
// import './js/main.js';
// import './js/wow.js';
// import './js/wow.min.js';

// Images
import hero from './img/hero.png';
import about from './img/about.png';
import class1 from './img/class-1.jpg';
import class2 from './img/class-2.jpg';
import class3 from './img/class-3.jpg';
import class4 from './img/class-4.jpg';
import class5 from './img/class-5.jpg';
import class6 from './img/class-5.jpg';
import teacher1 from './img/teacher-1.png';
import teacher2 from './img/teacher-2.png';
import teacher3 from './img/teacher-3.png';
import teacher4 from './img/teacher-4.png';
import teacher5 from './img/teacher-5.png';
import teacher6 from './img/teacher-6.png';
import testi1 from './img/testimonial-1.jpg';
import testi2 from './img/testimonial-2.jpg';
import testi3 from './img/testimonial-3.jpg';
import testi4 from './img/testimonial-4.jpg';
import team1 from './img/team-1.jpg';
import team2 from './img/team-2.jpg';
import team3 from './img/team-3.jpg';
import team4 from './img/team-4.jpg';
import Header from './components/Header';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
           <div className="top-bar d-none d-md-block">
              <div className="container-fluid">
                  <div className="row">
                      <div className="col-md-8">
                          <div className="top-bar-left">
                              <div className="text">
                                  <i className="far fa-clock"></i>
                                  <h2>8:00 - 9:00</h2>
                                  <p>Mon - Fri</p>
                              </div>
                              <div className="text">
                                  <i className="fa fa-phone-alt"></i>
                                  <h2>+123 456 7890</h2>
                                  <p>For Appointment</p>
                              </div>
                          </div>
                      </div>
                      <div className="col-md-4">
                          <div className="top-bar-right">
                              <div className="social">
                                  <a href="#"><i className="fab fa-twitter"></i></a>
                                  <a href="#"><i className="fab fa-facebook-f"></i></a>
                                  <a href="#"><i className="fab fa-linkedin-in"></i></a>
                                  <a href="#"><i className="fab fa-instagram"></i></a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div className="navbar navbar-expand-lg bg-dark navbar-dark">
              <div className="container-fluid">
                  <a href="index.html" className="navbar-brand">Y<span>oo</span>ga</a>
                  <button type="button" className="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                      <span className="navbar-toggler-icon"></span>
                  </button>
    
                  <div className="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                      <div className="navbar-nav ml-auto">
                          <a href="index.html" className="nav-item nav-link active">Home</a>
                          <a href="about.html" className="nav-item nav-link">About</a>
                          <a href="service.html" className="nav-item nav-link">Service</a>
                          <a href="price.html" className="nav-item nav-link">Price</a>
                          <a href="className.html" className="nav-item nav-link">className</a>
                          <a href="team.html" className="nav-item nav-link">Trainer</a>
                          <a href="portfolio.html" className="nav-item nav-link">Pose</a>
                          <div className="nav-item dropdown">
                              <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown">Blog</a>
                              <div className="dropdown-menu">
                                  <a href="blog.html" className="dropdown-item">Blog Grid</a>
                                  <a href="single.html" className="dropdown-item">Blog Detail</a>
                              </div>
                          </div>
                          <a href="contact.html" className="nav-item nav-link">Contact</a>
                      </div>
                  </div>
              </div>
          </div>
          <div class="hero">
              <div className="container-fluid">
                  <div className="row align-items-center">
                      <div className="col-sm-12 col-md-6">
                          <div className="hero-text">
                              <h1>Yoga Practice For Good Health</h1>
                              <p>
                                  Lorem ipsum dolor sit amet elit. Phasell nec pretum mi. Curabi ornare velit non. Aliqua metus tortor auctor quis sem.
                              </p>
                              <div className="hero-btn">
                                  <a className="btn" href="#">Join Now</a>
                                  <a className="btn" href="#">Contact Us</a>
                              </div>
                          </div>
                      </div>
                      <div className="col-sm-12 col-md-6 d-none d-md-block">
                          <div className="hero-image"> 
                                <img src={hero} alt="Hero"></img>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div className="about wow fadeInUp" data-wow-delay="0.1s">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-5 col-md-6">
                        <div className="about-img">
                         <img src={about} alt="about"></img>                           
                        </div>
                    </div>
                    <div className="col-lg-7 col-md-6">
                        <div className="section-header text-left">
                            <p>Learn About Us</p>
                            <h2>Welcome to Yooga</h2>
                        </div>
                        <div className="about-text">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non vulputate. Aliquam metus tortor, auctor id gravida condimentum, viverra quis sem.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non vulputate. Aliquam metus tortor, auctor id gravida condimentum, viverra quis sem. Curabitur non nisl nec nisi scelerisque maximus.
                            </p>
                            <a className="btn" href="">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div className="service">
            <div className="container">
                <div className="section-header text-center wow zoomIn" data-wow-delay="0.1s">
                    <p>What we do</p>
                    <h2>Yoga For Health</h2>
                </div>
                <div className="row">
                    <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.0s">
                        <div className="service-item">
                            <div className="service-icon">
                                <i className="flaticon-workout"></i>
                            </div>
                            <h3>Heal emotions</h3>
                            <p>
                                Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div className="service-item active">
                            <div className="service-icon">
                                <i className="flaticon-workout-1"></i>
                            </div>
                            <h3>Body Refreshes</h3>
                            <p>
                                Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                        <div classNaem="service-item">
                            <div className="service-icon">
                                <i className="flaticon-workout-2"></i>
                            </div>
                            <h3>Mind & Serenity</h3>
                            <p>
                                Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                        <div className="service-item">
                            <div className="service-icon">
                                <i className="flaticon-workout-3"></i>
                            </div>
                            <h3>Enjoy Your life</h3>
                            <p>
                                Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.8s">
                        <div className="service-item">
                            <div className="service-icon">
                                <i className="flaticon-workout-4"></i>
                            </div>
                            <h3>Body & Spirituality</h3>
                            <p>
                                Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="1s">
                        <div className="service-item">
                            <div className="service-icon">
                                <i className="flaticon-yoga-pose"></i>
                            </div>
                            <h3>Body & Mind</h3>
                            <p>
                                Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

                <div className="class">
            <div className="container">
                <div className="section-header text-center wow zoomIn" data-wow-delay="0.1s">
                    <p>Our Classes</p>
                    <h2>Yoga Class Shedule</h2>
                </div>
                <div className="row">
                    <div className="col-12">
                        <ul id="class-filter">
                            <li data-filter="*" class="filter-active">All Classes</li>
                            <li data-filter=".filter-1">Pilates Yoga</li>
                            <li data-filter=".filter-2">Hatha Yoga</li>
                            <li data-filter=".filter-3">Vinyasa yoga</li>
                        </ul>
                    </div>
                </div>
                <div className="row class-container">
                    <div className="col-lg-4 col-md-6 col-sm-12 class-item filter-1 wow fadeInUp" data-wow-delay="0.0s">
                        <div className="class-wrap">
                            <div className="class-img">
                                <img src={class1} alt="class-1"></img>
                            </div>
                            <div className="class-text">
                                <div className="class-teacher"> 
                                    <img src={teacher1} alt=""></img>
                                    <h3>Elise Moran</h3>
                                    <a href="">+</a>
                                </div>
                                <h2>Pilates Yoga</h2>
                                <div className="class-meta">
                                    <p><i className="far fa-calendar-alt"></i>Sun, Tue, Thu</p>
                                    <p><i className="far fa-clock"></i>9:00 - 10:00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-12 class-item filter-2 wow fadeInUp" data-wow-delay="0.2s">
                        <div className="class-wrap">
                            <div className="class-img">
                                <img src={class2} alt="class-2"></img>
                            </div>
                            <div className="class-text">
                                <div className="class-teacher">
                                    <img src={teacher2} alt="teacher-2"></img>
                                    <h3>Kate Glover</h3>
                                    <a href="">+</a>
                                </div>
                                <h2>Iyengar Yoga</h2>
                                <div className="class-meta">
                                    <p><i className="far fa-calendar-alt"></i>Sun, Tue, Thu</p>
                                    <p><i className="far fa-clock"></i>9:00 - 10:00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-12 class-item filter-3 wow fadeInUp" data-wow-delay="0.4s">
                        <div className="class-wrap">
                            <div className="class-img">
                                <img src={class3} alt="class-3"></img>
                            </div>
                            <div className="class-text">
                                <div className="class-teacher">
                                    <img src={teacher3} alt="teacher-3"></img>
                                    <h3>Elina Ekman</h3>
                                    <a href="">+</a>
                                </div>
                                <h2>Ashtanga yoga</h2>
                                <div class="class-meta">
                                    <p><i className="far fa-calendar-alt"></i>Sun, Tue, Thu</p>
                                    <p><i className="far fa-clock"></i>9:00 - 10:00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-12 class-item filter-1 wow fadeInUp" data-wow-delay="0.6s">
                        <div className="class-wrap">
                            <div className="class-img">
                                <img src={class4} alt="class-4"></img>
                            </div>
                            <div className="class-text">
                                <div className="class-teacher">
                                    <img src={teacher4} alt="teacher-4"></img>
                                    <h3>Lilly Fry</h3>
                                    <a href="">+</a>
                                </div>
                                <h2>Hatha Yoga</h2>
                                <div class="class-meta">
                                    <p><i className="far fa-calendar-alt"></i>Sun, Tue, Thu</p>
                                    <p><i className="far fa-clock"></i>9:00 - 10:00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-12 class-item filter-2 wow fadeInUp" data-wow-delay="0.8s">
                        <div className="class-wrap">
                            <div className="class-img">
                                <img src={class5} alt="class-5"></img>                            </div>
                            <div className="class-text">
                                <div className="class-teacher">
                                    <img src={teacher5} alt="teacher-5"></img>
                                    <h3>Olivia Yates</h3>
                                    <a href="">+</a>
                                </div>
                                <h2>Kundalini Yoga</h2>
                                <div className="class-meta">
                                    <p><i className="far fa-calendar-alt"></i>Sun, Tue, Thu</p>
                                    <p><i className="far fa-clock"></i>9:00 - 10:00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-12 class-item filter-3 wow fadeInUp" data-wow-delay="1s">
                        <div className="class-wrap">
                            <div className="class-img">
                                <img src={class6} alt="class-6"></img>
                            </div>
                            <div className="class-text">
                                <div className="class-teacher">
                                    <img src={teacher6} alt="teacher-6"></img>
                                    <h3>Millie Harper</h3>
                                    <a href="">+</a>
                                </div>
                                <h2>Vinyasa yoga</h2>
                                <div className="class-meta">
                                    <p><i className="far fa-calendar-alt"></i>Sun, Tue, Thu</p>
                                    <p><i className="far fa-clock"></i>9:00 - 10:00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="discount wow zoomIn" data-wow-delay="0.1s">
            <div className="container">
                <div className="section-header text-center">
                    <p>Awesome Discount</p>
                    <h2>Get <span>30%</span> Discount for all Classes</h2>
                </div>
                <div className="container discount-text">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non vulputate. Aliquam metus tortor, auctor id gravida condimentum, viverra quis sem. Curabitur non nisl nec nisi scelerisque maximus. 
                    </p>
                    <a className="btn">Join Now</a>
                </div>
            </div>
        </div>
        <div className="price">
            <div className="container">
                <div className="section-header text-center wow zoomIn" data-wow-delay="0.1s">
                    <p>Yoga Package</p>
                    <h2>Yoga Pricing Plan</h2>
                </div>
                <div className="row">
                    <div className="col-md-4 wow fadeInUp" data-wow-delay="0.0s">
                        <div className="price-item">
                            <div className="price-header">
                                <div className="price-title">
                                    <h2>Basic</h2>
                                </div>
                                <div className="price-prices">
                                    <h2><small>$</small>49<span>/ mo</span></h2>
                                </div>
                            </div>
                            <div className="price-body">
                                <div className="price-description">
                                    <ul>
                                        <li>Personal Trainer</li>
                                        <li>Special Class</li>
                                        <li>Free Tutorials</li>
                                        <li>Group Training</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="price-footer">
                                <div className="price-action">
                                    <a className="btn" href="">Join Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 wow fadeInUp" data-wow-delay="0.3s">
                        <div className="price-item featured-item">
                            <div className="price-header">
                                <div className="price-status">
                                    <span>Popular</span>
                                </div>
                                <div className="price-title">
                                    <h2>Standard</h2>
                                </div>
                                <div className="price-prices">
                                    <h2><small>$</small>99<span>/ mo</span></h2>
                                </div>
                            </div>
                            <div className="price-body">
                                <div className="price-description">
                                    <ul>
                                        <li>Personal Trainer</li>
                                        <li>Special Class</li>
                                        <li>Free Tutorials</li>
                                        <li>Group Training</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="price-footer">
                                <div className="price-action">
                                    <a className="btn" href="">Join Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                        <div className="price-item">
                            <div className="price-header">
                                <div className="price-title">
                                    <h2>Premium</h2>
                                </div>
                                <div className="price-prices">
                                    <h2><small>$</small>149<span>/ mo</span></h2>
                                </div>
                            </div>
                            <div className="price-body">
                                <div className="price-description">
                                    <ul>
                                        <li>Personal Trainer</li>
                                        <li>Special Class</li>
                                        <li>Free Tutorials</li>
                                        <li>Group Training</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="price-footer">
                                <div className="price-action">
                                    <a className="btn" href="">Join Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="testimonial wow fadeInUp" data-wow-delay="0.1s">
            <div className="container">
                <div className="section-header text-center">
                    <p>Testimonial</p>
                    <h2>Our Client Say!</h2>
                </div>
                <div className="owl-carousel testimonials-carousel">
                    <div className="testimonial-item">
                        <div className="testimonial-img">
                            <img src={testi1} alt="test-1"></img>
                        </div>
                        <div className="testimonial-text">
                            <p>
                                Lorem ipsum dolor sit amet consec adipis elit. Etiam accums lacus eget velit tincid, quis suscip justo dictum.
                            </p>
                            <h3>Customer Name</h3>
                            <h4>Profession</h4>
                        </div>
                    </div>
                    <div className="testimonial-item">
                        <div className="testimonial-img">
                             <img src={testi2} alt="test-2"></img>    
                        </div>
                        <div className="testimonial-text">
                            <p>
                                Lorem ipsum dolor sit amet consec adipis elit. Etiam accums lacus eget velit tincid, quis suscip justo dictum.
                            </p>
                            <h3>Customer Name</h3>
                            <h4>Profession</h4>
                        </div>
                    </div>
                    <div className="testimonial-item">
                        <div className="testimonial-img">
                            <img src={testi3} alt="test-3"></img>   
                        </div>
                        <div className="testimonial-text">
                            <p>
                                Lorem ipsum dolor sit amet consec adipis elit. Etiam accums lacus eget velit tincid, quis suscip justo dictum.
                            </p>
                            <h3>Customer Name</h3>
                            <h4>Profession</h4>
                        </div>
                    </div>
                    <div className="testimonial-item">
                        <div className="testimonial-img">
                            <img src={testi4} alt="testi-4"></img>   
                        </div>
                        <div className="testimonial-text">
                            <p>
                                Lorem ipsum dolor sit amet consec adipis elit. Etiam accums lacus eget velit tincid, quis suscip justo dictum.
                            </p>
                            <h3>Customer Name</h3>
                            <h4>Profession</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>       
        <div className="team">
            <div className="container">
                <div className="section-header text-center wow zoomIn" data-wow-delay="0.1s">
                    <p>Yoga Trainer</p>
                    <h2>Expert Yoga Trainer</h2>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.0s">
                        <div className="team-item">
                            <div className="team-img">
                                <img src={team1} alt="team-1"></img>
                                <div className="team-social">
                                    <a href=""><i className="fab fa-twitter"></i></a>
                                    <a href=""><i className="fab fa-facebook-f"></i></a>
                                    <a href=""><i className="fab fa-linkedin-in"></i></a>
                                    <a href=""><i className="fab fa-instagram"></i></a>
                                </div>
                            </div>
                            <div className="team-text">
                                <h2>Millie Harper</h2>
                                <p>Yoga Teacher</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div className="team-item">
                            <div className="team-img">
                                <img src={team2} alt="team-2"></img>
                                <div class="team-social">
                                    <a href=""><i className="fab fa-twitter"></i></a>
                                    <a href=""><i className="fab fa-facebook-f"></i></a>
                                    <a href=""><i className="fab fa-linkedin-in"></i></a>
                                    <a href=""><i className="fab fa-instagram"></i></a>
                                </div>
                            </div>
                            <div className="team-text">
                                <h2>Lilly Fry</h2>
                                <p>Yoga Teacher</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                        <div className="team-item">
                            <div className="team-img">
                                 <img src={team3} alt="Team-3"></img>
                                <div className="team-social">
                                    <a href=""><i className="fab fa-twitter"></i></a>
                                    <a href=""><i className="fab fa-facebook-f"></i></a>
                                    <a href=""><i className="fab fa-linkedin-in"></i></a>
                                    <a href=""><i className="fab fa-instagram"></i></a>
                                </div>
                            </div>
                            <div className="team-text">
                                <h2>Elise Moran</h2>
                                <p>Yoga Teacher</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                        <div className="team-item">
                            <div className="team-img">
                                 <img src={team4} alt="Team-4"></img>
                                <div className="team-social">
                                    <a href=""><i className="fab fa-twitter"></i></a>
                                    <a href=""><i className="fab fa-facebook-f"></i></a>
                                    <a href=""><i className="fab fa-linkedin-in"></i></a>
                                    <a href=""><i className="fab fa-instagram"></i></a>
                                </div>
                            </div>
                            <div className="team-text">
                                <h2>Kate Glover</h2>
                                <p>Yoga Teacher</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
      
    );
  }
}
